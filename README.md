# Repositorio imagenes docker

Al utilizar docker me he encontrado que cuesta configurar las imagenes por defecto. Debido a ello, poco a poco estoy creando mis propias imagenes con pequeños cambios desde las oficiales.

Este proyecto es para tener guardados los scripts y los Dockerfiles con las pequeñas modificaciones que he realizado. Se hace público para que cualquiera pueda contribuir, aportar sus ideas, etc...